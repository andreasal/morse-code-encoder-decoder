#morse.js

Easy JavaScript morse encoder and decoder.

##Usage:

    var str = "Hello World",
        encoded = morse.encode(str),
        decoded = morse.decode(encoded);

    console.log(encoded);
    console.log(decoded);